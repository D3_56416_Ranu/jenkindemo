const {response}=require('express')
const express=require('express')
const {request}=require('https')
const databse=require('mime-db')
const mysql=require('mysql2')
const cors=require('cors')
const exp = require('constants')

const app=express()
app.use(cors('*'))
app.use(express.json())

const openDatabaseConnection=()=>{
    const connection=mysql.createConnection({
        port:3306,
        host:'localhost',
        user:'root',
        password:'manager',
        database:'db'
    })
    connection.connect()
    return connection
}
//add 
app.post('/add',(request,response)=>{
    const connection=openDatabaseConnection()
    const {id, name, age}=request.body
    const statement=`insert into Person values (${id},'${name}','${age}')`

    connection.query(statement,(error,result)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})

//display 
app.get('/show',(request,response)=>{
    const connection=openDatabaseConnection()
    const {name}=request.body
    const statement=`select * from Person where name='${name}'`

    connection.query(statement,(error,result)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})

//update 
app.put('/update',(request,response)=>{
    const connection=openDatabaseConnection()
    const { id,age}=request.body
    const statement=`update Person set age=${age} where id=${id}`

    connection.query(statement,(error,result)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})

//delete 
app.delete('/delete',(request,response)=>{
    const connection=openDatabaseConnection()
    const { movie_title}=request.body
    const statement=`delete from  Person  where id='${id}'`

    connection.query(statement,(error,result)=>{
        connection.end()
        if(error)
        {
            response.send(error)
        }
        else{
            response.send(result)
        }
    })
})


app.listen(4000,'0.0.0.0',()=>{
    console.log(`server started on port 4000`)
})